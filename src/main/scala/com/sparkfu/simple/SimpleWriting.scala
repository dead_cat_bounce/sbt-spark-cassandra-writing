package com.sparkfu.simple

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import com.datastax.spark.connector._
import com.datastax.spark.connector.rdd._
import org.apache.spark.rdd._
import com.datastax.spark.connector.cql.CassandraConnector
import scala.reflect.runtime.universe

object Writer {
  
  // Our simple case classes for human.
  case class Human(id:String, firstname:String, lastname:String)
  case class SimpleHuman(firstname:String, lastname:String)
  case class CallableHuman(fname:String, lname:String, gender:String, phone:String)

  def main(args: Array[String]) {
    val conf = new SparkConf(true)
      .set("spark.cassandra.connection.host", "127.0.0.1")

    val sc = new SparkContext("spark://127.0.0.1:7077", "sparkfu", conf)
    
    // Fetch only the details we need and only for good people.
    val goodHumans:CassandraRDD[Human] = sc.cassandraTable[Human]("sparkfu", "human").select("id", "firstname", "lastname").where("isgoodperson = True")
    
    // Let's transform our "complex" humans to a RDD full of simplified humans 
    val goodSimpleHumans:RDD[SimpleHuman] = goodHumans.map(h =>  SimpleHuman(h.firstname, h.lastname))
    
    // Now let's save our collection of good, simple humans off to Cassandra.
    val x:Unit = goodSimpleHumans.saveToCassandra("sparkfu", "goodhuman", SomeColumns("firstname", "lastname"))
    
    // Verfiy by fetching our list of good humans and printing out their names.
    val theGoodOnes:CassandraRDD[SimpleHuman] = sc.cassandraTable[SimpleHuman]("sparkfu", "goodhuman").select("firstname", "lastname")
    theGoodOnes.toArray.foreach { goodHuman:SimpleHuman =>
        println("%s %s is a good, simple person.".format(goodHuman.firstname, goodHuman.lastname))
    }
    
    ////////////////////////
    // Wide tuple example //
    ////////////////////////
    
    // Fetch all but the kitchen sink...
    // Also note we omit 'country' as our target table does not have that.
    // Alternatively you could perform a transformation to remove 'country' - but, let's keep it simple for now.
    val wideTuple: CassandraRDD[(String, String, String, String, String, String, String, String, String, String)] = 
      sc.cassandraTable[(String, String, String, String, String, String, String, String, String, String)]("sparkfu", "human")
      .select("id", "firstname", "lastname", "gender", "address0", "address1", "city", "stateprov", "zippostal", "phone")
      .where("isgoodperson = True")
      
    // We'll omit any clever transformations for now as all we really want to illustrate is how to map this wide tuple
    // onto 'humanfromtuple' table which has many different column names from our 'human' table.
    // HUMAN     -> HUMANFROMTUPLE
    // id        -> id
    // firstname -> fname *
    // lastname  -> lname *
    // gender    -> gender
    // address0  -> address1 *
    // address1  -> address2 *
    // city      -> city
    // stateprov -> state *
    // zippostal -> zip *
    // country   -> country
    // phone     -> phone
    
    // So let's save our wide tuple of good humans off to Cassandra.
    val y:Unit = wideTuple.saveToCassandra("sparkfu", "humanfromtuple", SomeColumns(
      // NOTE: Name, number of elements, and order must align precisely between your tuple and your target table!!!
      "id", "fname", "lname", "gender", "address1", "address2", "city", "state", "zip", "phone")
    )
    
    // ...and since we cannot just assume that all worked - let's pull some of those records back and print them out.
    val personsToCall:CassandraRDD[CallableHuman] = sc.cassandraTable[CallableHuman]("sparkfu", "humanfromtuple").select("fname", "lname", "gender", "phone")
    
    personsToCall.toArray.foreach { ch:CallableHuman =>
      val genderVerbose:String = ch.gender match {
        case "m" => "male"
        case "f" => "female"
        case "t" => "transgender"
        case _ => "unknown"
      }
      
      println("%s %s is %s and can be reached at %s.".format(ch.fname, ch.lname, genderVerbose, ch.phone))
    }
    
    // Clean up
    // Comment this out if you'd like to validate your results inside Cassandra.
    CassandraConnector(conf).withSessionDo { session =>
      session.execute("TRUNCATE sparkfu.goodhuman")
      session.execute("TRUNCATE sparkfu.humanfromtuple")
    }
    
  }
}